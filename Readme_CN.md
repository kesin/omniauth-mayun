# Omniauth::Mayun

Gitee 的 Omniauth 登录Gem包。

# 代码仓库

码云 http://git.oschina.net/kesin/omniauth-mayun

Github https://github.com/kesin/omniauth-mayun

RubyGems https://rubygems.org/gems/omniauth-mayun

## 安装

在 Gemfile 添加：

    gem 'omniauth-mayun'

执行：

    $ bundle

或者使用 gem 命令直接安装：

    $ gem install omniauth-mayun

## 使用

配置 Omniauth 即可

## 贡献

1. Fork本项目
2. 创建你的开发分支 (`git checkout -b my-new-feature`)
3. 将改动提交 (`git commit -am 'Add some feature'`)
4. 推送到分支上 (`git push origin my-new-feature`)
5. 创建 PullRequest